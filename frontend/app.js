// Based on https://github.com/john-smilga/javascript-basic-projects/blob/master/14-grocery-bud/final/app.js

// select DOM items
const form = document.querySelector('.grocery-form');
const grocery = document.getElementById('grocery');
const list = document.querySelector('.grocery-list');
const container = document.querySelector('.grocery-container');
const alert = document.querySelector('.alert');
const clearButton = document.querySelector('.clear-btn');
const submitBtn = document.querySelector('.submit-btn');

let editElement;
let editId = '';
let editFlag = false;
// ******************************************************************************************
// F U N C T I O N S
// ******************************************************************************************

// reset form to default
function setToDefault() {
  grocery.value = '';
  editFlag = false;
  editId = '';
  submitBtn.textContent = 'odeslat';
}
// display items on load, reading from storage
function setupItems() {
  let items = getLocalStorage();
  if (items.length > 0) {
    items.forEach((item) => {
      const element = createListItem(item.id, item.value);
      list.appendChild(element);
    });
    container.classList.add('show-container');
  }
}

// clear all items from GUI and from the storage as well
function clearItems() {
  // clear GUI
  const items = document.querySelectorAll('.grocery-item');
  if (items.length > 0) {
    items.forEach((item) => {
      list.removeChild(item);
    });
  }
  container.classList.remove('show-container');
  displayAlert('prázdný seznam', 'danger');
  setToDefault();
  // clear storage
  localStorage.removeItem('list');
}

// add item to the list
// event handler
function addOrEditItem(e) {
  e.preventDefault();
  const value = grocery.value;
  if (value !== '' && !editFlag) {
    const id = new Date().getTime().toString();
    const element = createListItem(id, value);
    list.appendChild(element);
    displayAlert('položka byla přidaná do listu', 'success');
    container.classList.add('show-container');
    addToLocalStorage(id, value);
    setToDefault();
  } else if (value !== '' && editFlag) {
    editElement.textContent = value;
    displayAlert('položka zmeněna', 'success');
    editLocalStorage(editId, value);
    setToDefault();
  } else {
    displayAlert('napsat položku seznamu', 'danger');
  }
}

// ***************************************************************************
// assigning event listeners
// ****************************

// submit form
form.addEventListener('submit', addOrEditItem);

// clear list
clearButton.addEventListener('click', clearItems);

// display items on load
window.addEventListener('DOMContentLoaded', setupItems);

// *****************************************************************************

// ************************
// LOCAL STORAGE
// ************************
// CRUD - create
function addToLocalStorage(id, value) {
  const grocery = { id, value };
  let items = getLocalStorage();
  items.push(grocery);
  localStorage.setItem('list', JSON.stringify(items));
}

// CRUD - delete
function removeFromLocalStorage(id) {
  let items = getLocalStorage();
  items = items.filter((item) => {
    if (item.id !== id) {
      return item;
    }
  });
  localStorage.setItem('list', JSON.stringify(items));
}

// CRUD - read
function getLocalStorage() {
  let items = [];
  const itemsJSON = localStorage.getItem('list');
  if (itemsJSON !== null) {
    items = JSON.parse(itemsJSON);
  }
  return items;
}
// CRUD - update
function editLocalStorage(id, value) {
  let items = getLocalStorage();
  items = items.map((item) => {
    if (item.id === id) {
      item.value = value;
    }
    return item;
  });
  localStorage.setItem('list', JSON.stringify(items));
}

// ******************************************************************************
// messaging to user function
// ***************************

// display alert
function displayAlert(alertText, alertType) {
  alert.textContent = alertText;
  const className = `alert-${alertType}`;
  alert.classList.add(className);
  setTimeout(() => {
    alert.textContent = '';
    alert.classList.remove(className);
  }, 1000);
}

// ****************************
// DOM manipulation functions
// ****************************

// create DOM element containing grocery item entered by user with edit and delete buttons
function createListItem(id, value) {
  const element = document.createElement('article');
  let attr = document.createAttribute('data-id');
  attr.value = id;
  element.setAttributeNode(attr);
  element.classList.add('grocery-item');
  const title = document.createElement('p');
  title.textContent = value;
  title.classList.add('title');
  element.appendChild(title);

  const btnContainer = document.createElement('div');
  btnContainer.classList.add('btn-container');
  const editButton = document.createElement('button');
  editButton.classList.add('edit-btn');
  const faEdit = document.createElement('i');
  faEdit.classList.add('fas', 'fa-edit');
  const deleteButton = document.createElement('button');
  deleteButton.classList.add('delete-btn');
  const faDelete = document.createElement('i');
  faDelete.classList.add('fas', 'fa-trash');

  editButton.type = 'button';
  deleteButton.type = 'button';

  editButton.addEventListener('click', editItem);
  deleteButton.addEventListener('click', deleteItem);

  editButton.appendChild(faEdit);
  deleteButton.appendChild(faDelete);
  btnContainer.appendChild(editButton);
  btnContainer.appendChild(deleteButton);
  element.appendChild(btnContainer);

  return element;
}

// *********** event handler
// edit grocery item
function editItem(e) {
  const element = e.currentTarget.parentElement.parentElement;
  editElement = e.currentTarget.parentElement.previousElementSibling;
  grocery.value = editElement.textContent;
  editFlag = true;
  editId = element.dataset.id;
  submitBtn.textContent = 'upravit';
}

// *********** event handler
// delete grocery item
function deleteItem(e) {
  const element = e.currentTarget.parentElement.parentElement;
  const id = element.dataset.id;
  list.removeChild(element);
  if (list.children.length === 0) {
    container.classList.remove('show-container');
  }
  displayAlert('položka smazaná', 'danger');
  setToDefault();
  removeFromLocalStorage(id);
}
