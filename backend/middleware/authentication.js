const Comment = require('../models/comment');

module.exports.isLoggedIn = (req, res, next) => {
  if (!req.isAuthenticated()) {
    return res.redirect('/login');
  } else {
    next();
  }
};

module.exports.isAuthor = async (req, res, next) => {
  const { id } = req.params;
  const comment = await Comment.findById(id);
  if (!req.user._id.equals(comment.author)) {
    return res.redirect(`/comments/${id}`);
  } else {
    next();
  }
};
