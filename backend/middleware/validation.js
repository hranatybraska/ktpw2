const { commentSchema } = require('../schemas');
const ExpressError = require('../utils/ExpressError');

module.exports.validateComment = (req, res, next) => {
  const { error } = commentSchema.validate(req.body);
  if (error) {
    const message = error.details.map((el) => el.message).join(', ');
    throw new ExpressError(message, 400);
  } else {
    next();
  }
};
