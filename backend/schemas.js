const Joi = require('joi');

module.exports.commentSchema = Joi.object({
  comment: Joi.object({
    title: Joi.string().required(),
    comment: Joi.string().required(),
    //price: Joi.number().required(),
  }).required(),
});
