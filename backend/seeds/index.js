const mongoose = require('mongoose');
const Comment = require('../models/comment');

mongoose.set('useNewUrlParser', true);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

mongoose
  .connect('mongodb://127.0.0.1:27017/commentsKTPW2')
  .catch((err) => console.log(err));

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error'));

db.once('open', () => {
  console.log('Database connected');
});

const comments = [
  {
    title: 'favorite book',
    comment: 'My favourite book is Winnie the Pooh.',
  },
  {
    title: 'shopping',
    comment: 'Not forget shopping today!',
  },
  {
    title: 'personal note',
    comment: 'Dad has birthday on Saturday!',
  },
  {
    title: 'to do',
    comment: 'Prepare lectures for Monday.',
  },
];

const seedDB = async () => {
  await Comment.insertMany(comments);
};

seedDB()
  .then(() => {
    mongoose.connection.close();
    console.log('Writing to DB successful, DB disconnected');
  })
  .catch((err) => {
    console.log('error while writing to DB');
  });
